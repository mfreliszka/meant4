import restful_booker, requests
import pytest

def test_add_valid_booking():
    resp = restful_booker.add_valid_booking()
    assert resp.status_code == requests.codes.ok, "Valid booking should be possible!"

def test_add_booking_without_additionalneeds():
    resp = restful_booker.add_booking_without_additionalneeds()
    assert resp.status_code == requests.codes.ok, "booking without additional needs should be possible!"

"""   Negative tests   """

def test_add_booking_without_firstname():
    resp = restful_booker.add_booking_without_firstname()
    assert resp.status_code == requests.codes.internal_server_error, "Booking without first name shouldn't be possible!"

def test_add_booking_without_lastname():
    resp = restful_booker.add_booking_without_lastname()
    assert resp.status_code == requests.codes.internal_server_error, "Booking without last name shouldn't be possible!"

def test_add_booking_without_totalprice():
    resp = restful_booker.add_booking_without_totalprice()
    assert resp.status_code == requests.codes.internal_server_error, "Booking without total price shouldn't be possible!"

def test_add_booking_without_depositpaid():
    resp = restful_booker.add_booking_without_depositpaid()
    assert resp.status_code == requests.codes.internal_server_error, "Booking without deposit paid information shouldn't be possible!"

def test_add_booking_without_checkin():
    resp = restful_booker.add_booking_without_checkin()
    assert resp.status_code == requests.codes.internal_server_error, "Booking without checkin date shouldn't be possible!"

def test_add_booking_without_checkout():
    resp = restful_booker.add_booking_without_checkout()
    assert resp.status_code == requests.codes.internal_server_error, "Booking without checkout date shouldn't be possible!"

def test_add_booking_without_bookingdates():
    resp = restful_booker.add_booking_without_bookingdates()
    assert resp.status_code == requests.codes.internal_server_error, "Booking without booking dates shouldn't be possible!"

def test_booking_with_checkout_before_checkin():
    resp = restful_booker.add_booking_with_checkout_before_checkin()
    assert resp.status_code == requests.codes.internal_server_error, "Booking with Checkout before Checkin shouldn't be possible!"
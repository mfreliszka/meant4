from booking_generator import (
    valid_booking,
    booking_without_firstname,
    booking_without_lastname,
    booking_without_checkin, booking_without_checkout,
    booking_without_bookingdates, booking_without_additionalneeds,
    booking_without_depositpaid,
    booking_without_totalprice,
    booking_with_checkout_before_checkin
)
import requests

def _url(path):
    return "https://restful-booker.herokuapp.com" + path

def add_booking(booking):
    return requests.post(_url('/booking/'), json=booking)

def add_valid_booking():
    return add_booking(valid_booking())

def add_booking_without_firstname():
    return add_booking(booking_without_firstname())

def add_booking_without_lastname():
    return add_booking(booking_without_lastname())

def add_booking_without_checkin():
    return add_booking(booking_without_checkin())

def add_booking_without_checkout():
    return add_booking(booking_without_checkout())

def add_booking_without_bookingdates():
    return add_booking(booking_without_bookingdates())

def add_booking_without_additionalneeds():
    return add_booking(booking_without_additionalneeds())

def add_booking_without_depositpaid():
    return add_booking(booking_without_depositpaid())

def add_booking_without_totalprice():
    return add_booking(booking_without_totalprice())

def add_booking_with_checkout_before_checkin():
    return add_booking(booking_with_checkout_before_checkin())
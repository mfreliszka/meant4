from random import choice

def valid_booking():
    firstname = choice(['Michal','Jan','Tomasz','Adam','Krzysztof','Andrzej'])
    lastname = choice(['Kowalski','Nowak','Duda','Kwasniewski','Kowalczyk'])
    price = choice([270,410,190,130,500])
    deposit = choice([True,False])
    checkin = choice(['2020-07-04','2020-07-11','2020-04-01','2020-05-01','2020-01-02'])
    checkout = choice(['2021-07-09','2021-07-18','2021-04-15','2021-05-08','2021-01-06'])
    additionalneeds = choice(['Sandwich','Salad','Beer','Wine','Drink'])

    return({
        'firstname': firstname,
        'lastname': lastname,
        'totalprice': price,
        'depositpaid': deposit,
        'bookingdates':{
            'checkin': checkin,
            'checkout':checkout
        },
        'additionalneeds': additionalneeds
    })

def booking_without_firstname():
    lastname = choice(['Kowalski','Nowak','Duda','Kwasniewski','Kowalczyk'])
    price = choice([270,410,190,130,500])
    deposit = choice([True,False])
    checkin = choice(['2020-07-04','2020-07-11','2020-04-01','2020-05-01','2020-01-02'])
    checkout = choice(['2021-07-09','2021-07-18','2021-04-15','2021-05-08','2021-01-06'])
    additionalneeds = choice(['Sandwich','Salad','Beer','Wine','Drink'])

    return({
        'lastname': lastname,
        'totalprice': price,
        'depositpaid': deposit,
        'bookingdates':{
            'checkin': checkin,
            'checkout':checkout
        },
        'additionalneeds': additionalneeds
    })

def booking_without_lastname():
    firstname = choice(['Michal','Jan','Tomasz','Adam','Krzysztof','Andrzej'])
    price = choice([270,410,190,130,500])
    deposit = choice([True,False])
    checkin = choice(['2020-07-04','2020-07-11','2020-04-01','2020-05-01','2020-01-02'])
    checkout = choice(['2021-07-09','2021-07-18','2021-04-15','2021-05-08','2021-01-06'])
    additionalneeds = choice(['Sandwich','Salad','Beer','Wine','Drink'])

    return({
        'firstname': firstname,
        'totalprice': price,
        'depositpaid': deposit,
        'bookingdates':{
            'checkin': checkin,
            'checkout':checkout
        },
        'additionalneeds': additionalneeds
    })

def booking_without_checkin():
    firstname = choice(['Michal','Jan','Tomasz','Adam','Krzysztof','Andrzej'])
    lastname = choice(['Kowalski','Nowak','Duda','Kwasniewski','Kowalczyk'])
    price = choice([270,410,190,130,500])
    deposit = choice([True,False])
    checkout = choice(['2021-07-09','2011-07-18','2011-04-15','2011-05-08','2021-01-06'])
    additionalneeds = choice(['Sandwich','Salad','Beer','Wine','Drink'])

    return({
        'firstname': firstname,
        'lastname': lastname,
        'totalprice': price,
        'depositpaid': deposit,
        'bookingdates':{
            'checkout':checkout
        },
        'additionalneeds': additionalneeds
    })

def booking_without_checkout():
    firstname = choice(['Michal','Jan','Tomasz','Adam','Krzysztof','Andrzej'])
    lastname = choice(['Kowalski','Nowak','Duda','Kwasniewski','Kowalczyk'])
    price = choice([270,410,190,130,500])
    deposit = choice([True,False])
    checkin = choice(['2020-07-04','2020-07-11','2020-04-01','2020-05-01','2020-01-02'])
    additionalneeds = choice(['Sandwich','Salad','Beer','Wine','Drink'])

    return({
        'firstname': firstname,
        'lastname': lastname,
        'totalprice': price,
        'depositpaid': deposit,
        'bookingdates':{
            'checkin': checkin,
        },
        'additionalneeds': additionalneeds
    })

def booking_without_bookingdates():
    firstname = choice(['Michal','Jan','Tomasz','Adam','Krzysztof','Andrzej'])
    lastname = choice(['Kowalski','Nowak','Duda','Kwasniewski','Kowalczyk'])
    price = choice([270,410,190,130,500])
    deposit = choice([True,False])
    additionalneeds = choice(['Sandwich','Salad','Beer','Wine','Drink'])

    return({
        'firstname': firstname,
        'lastname': lastname,
        'totalprice': price,
        'depositpaid': deposit,
        'additionalneeds': additionalneeds
    })

def booking_without_additionalneeds():
    firstname = choice(['Michal','Jan','Tomasz','Adam','Krzysztof','Andrzej'])
    lastname = choice(['Kowalski','Nowak','Duda','Kwasniewski','Kowalczyk'])
    price = choice([270,410,190,130,500])
    deposit = choice([True,False])
    checkin = choice(['2020-07-04','2020-07-11','2020-04-01','2020-05-01','2020-01-02'])
    checkout = choice(['2021-07-09','2021-07-18','2021-04-15','2021-05-08','2021-01-06'])

    return({
        'firstname': firstname,
        'lastname': lastname,
        'totalprice': price,
        'depositpaid': deposit,
        'bookingdates':{
            'checkin': checkin,
            'checkout':checkout
        }
    })

def booking_without_depositpaid():
    firstname = choice(['Michal','Jan','Tomasz','Adam','Krzysztof','Andrzej'])
    lastname = choice(['Kowalski','Nowak','Duda','Kwasniewski','Kowalczyk'])
    price = choice([270,410,190,130,500])
    checkin = choice(['2020-07-04','2020-07-11','2020-04-01','2020-05-01','2020-01-02'])
    checkout = choice(['2021-07-09','2021-07-18','2021-04-15','2021-05-08','2021-01-06'])
    additionalneeds = choice(['Sandwich','Salad','Beer','Wine','Drink'])

    return({
        'firstname': firstname,
        'lastname': lastname,
        'totalprice': price,
        'bookingdates':{
            'checkin': checkin,
            'checkout':checkout
        },
        'additionalneeds': additionalneeds
    })

def booking_without_totalprice():
    firstname = choice(['Michal','Jan','Tomasz','Adam','Krzysztof','Andrzej'])
    lastname = choice(['Kowalski','Nowak','Duda','Kwasniewski','Kowalczyk'])
    deposit = choice([True,False])
    checkin = choice(['2020-07-04','2020-07-11','2020-04-01','2020-05-01','2020-01-02'])
    checkout = choice(['2021-07-09','2021-07-18','2021-04-15','2021-05-08','2021-01-06'])
    additionalneeds = choice(['Sandwich','Salad','Beer','Wine','Drink'])

    return({
        'firstname': firstname,
        'lastname': lastname,
        'depositpaid': deposit,
        'bookingdates':{
            'checkin': checkin,
            'checkout':checkout
        },
        'additionalneeds': additionalneeds
    })

# booking where checkout date is before checkin date
def booking_with_checkout_before_checkin():
    firstname = choice(['Michal','Jan','Tomasz','Adam','Krzysztof','Andrzej'])
    lastname = choice(['Kowalski','Nowak','Duda','Kwasniewski','Kowalczyk'])
    price = choice([270,410,190,130,500])
    deposit = choice([True,False])
    checkin = choice(['2020-08-04','2020-08-11','2020-08-01','2020-08-01','2020-08-02'])
    checkout = choice(['2020-07-09','2020-07-18','2020-04-15','2020-05-08','2020-01-06'])
    additionalneeds = choice(['Sandwich','Salad','Beer','Wine','Drink'])

    return({
        'firstname': firstname,
        'lastname': lastname,
        'totalprice': price,
        'depositpaid': deposit,
        'bookingdates':{
            'checkin': checkin,
            'checkout':checkout
        },
        'additionalneeds': additionalneeds
    })
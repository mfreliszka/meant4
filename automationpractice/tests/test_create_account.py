import pytest
from pytest_bdd import given, when, then, scenarios
from selenium import webdriver
from pages.auth_page import AuthPage

@pytest.fixture
def browser():
  driver = webdriver.Chrome(executable_path='./chromedriver.exe')
  yield driver
  driver.quit()

CONVERTERS = {
    'email' : str,
    'title' : str,
    'name' : str,
    'last_name' : str,
    'password' : str,
    'address' : str,
    'city' : str,
    'postal_code' : str,
    'phone' : str,
    'result' : str,
}

scenarios('../features/createaccount.feature', example_converters=CONVERTERS)

auth_page = AuthPage()

@given('The authentication page is displayed')
def disp_auth_page(browser):
    auth_page.load(browser)

@when('I type in <email> address, next I click on Create an account button')
def email_type_in(browser, email):
    auth_page.start_creating_account(browser, email)

@when('I set <title>, <name>, <last_name>, <password>, <address>, <city>, <postal_code>, <phone>, next I click Register button')
def credentials_type_in(browser, title, name, last_name, password, address, city, postal_code, phone):
    auth_page.register(browser, title, name, last_name, password, address, city, postal_code, phone)

@then('I should <result> register new account')
def verify_account_registration(browser, result):
    if result == 'successfully':
        title = auth_page.get_title(browser, auth_page.MY_ACCOUNT_PAGE_TITLE)
        assert title == auth_page.MY_ACCOUNT_PAGE_TITLE
    if result == 'unsuccessfully':
        assert auth_page.is_visible(browser,auth_page.REGISTRATION_ERROR)

@when('I type in invalid <email> address, next I click on Create an account button')
def inv_email_type_in(browser, email):
    auth_page.start_creating_account(browser, email)

@then('I should see Invalid email error')
def inv_email_error_verify(browser):
    assert auth_page.is_visible(browser,auth_page.CREATE_ACCOUNT_ERROR)
import pytest
from pytest_bdd import given, when, then, scenarios
from selenium import webdriver
from pages.home_page import HomePage
from pages.auth_page import AuthPage

@pytest.fixture
def browser():
  driver = webdriver.Chrome(executable_path='./chromedriver.exe')
  yield driver
  driver.quit()

scenarios('../features/shopping_flow.feature', example_converters=dict(email=str, password=str))

home_page = HomePage()
auth_page = AuthPage()

@given('I am logged in with correct <email> and <password>')
def do_login(browser, email, password):
    auth_page.load(browser)
    auth_page.do_login(browser, email, password)

@when('I go to home page')
def go_to_home_page(browser):
    home_page.load(browser)

@when('I hover on a product')
def product_hover(browser):
    home_page.hover_on_product(browser)

@when('I click on Add to cart button')
def add_product_to_cart(browser):
    home_page.add_product_to_cart_hover(browser)

@when('I click Proceed to checkout and go to Cart')
def go_to_cart_step(browser):
    home_page.proceed_to_checkout(browser)

@when('I click Proceed to checkout and go to Address step')
def go_to_address_step(browser):
    home_page.proceed_to_address(browser)

@when('I click I click Proceed to checkout and go to Shipping step')
def go_to_shipping_step(browser):
    home_page.proceed_to_shipping(browser)

@when('I click checkbox Terms of service')
def terms_checkbox_click(browser):
    home_page.terms_checkbox_check(browser)

@when('I click Proceed to checkout and go to Payment step')
def go_to_payment_step(browser):
    home_page.proceed_to_payment(browser)

@when('I click Pay by bank wire')
def choose_bank_wire(browser):
    home_page.choose_bank_wire(browser)

@when('I click confirm my order')
def confirm_order(browser):
    home_page.confirm_order(browser)

@then('A message Your order on My Store is complete should be displayed')
def verify_order_complete_message(browser):
    assert home_page.verify_order_complete_message(browser) == 'Your order on My Store is complete.'

@given('The home page is displayed')
def disp_home_page(browser):
    home_page.load(browser)

@when('I click Proceed to checkout and go to Sign in step')
def proceed_to_sign_in(browser):
    home_page.proceed_to_address(browser)
    
@when('I login with my <email> and <password> and click on Sign in')
def do_login(browser, email, password):
    auth_page.do_login(browser, email, password)


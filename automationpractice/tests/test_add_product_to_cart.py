import pytest
from pytest_bdd import given, when, then, scenarios
from selenium import webdriver
from pages.home_page import HomePage

@pytest.fixture
def browser():
  driver = webdriver.Chrome(executable_path='./chromedriver.exe')
  driver.maximize_window()
  yield driver
  driver.quit()

scenarios('../features/add_product_to_cart.feature')

home_page = HomePage()

@given('The home page is displayed')
def disp_main_page(browser):
    home_page.load(browser)

@when('I hover on product on page')
def hover_on_product(browser):
    home_page.hover_on_product(browser)

@when('I click Add to cart button')
def add_to_cart_hover(browser):
    home_page.add_product_to_cart_hover(browser)

@then('A message Product successfully added should be displayed')
def verify_message_visibility(browser):
    home_page.verify_product_added_successfully(browser)

@when('I click Quick view button')
def quick_view_click(browser):
    home_page.open_product_quick_view(browser)

@when('I click Add to cart button on quick view')
def add_to_cart_quick_view(browser):
    home_page.add_product_to_cart_quick_view(browser)

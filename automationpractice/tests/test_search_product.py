import pytest
from pytest_bdd import given, when, then, scenarios
from selenium import webdriver
from pages.home_page import HomePage

@pytest.fixture
def browser():
  driver = webdriver.Chrome(executable_path='./chromedriver.exe')
  yield driver
  driver.quit()

scenarios('../features/search_product.feature', example_converters=dict(string=str))

home_page = HomePage()

@given('The home page is displayed')
def disp_main_page(browser):
    home_page.load(browser)

@when('I click on search button with no string typed')
def do_search(browser):
    home_page.search_product(browser, "")

@then('I should see alert window Please enter a search keyword')
def alert_verify(browser):
    assert home_page.is_visible(browser, home_page.SEARCH_WARNING)

@when('I type <string> in search field and click on search button')
def do_search_with_string(browser, string):
    home_page.search_product(browser, string)
    
@then('I should see only products with <string> in title')
def verify_results(browser, string):
    assert home_page.check_search_results(browser, string)
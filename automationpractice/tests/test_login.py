import pytest
from pytest_bdd import given, when, then, scenarios
from selenium import webdriver
from pages.auth_page import AuthPage

@pytest.fixture
def browser():
  driver = webdriver.Chrome(executable_path='./chromedriver.exe')
  yield driver
  driver.quit()

scenarios('../features/login.feature', example_converters=dict(email=str, password=str, result=str))

auth_page = AuthPage()

@given('The authentication page is displayed')
def disp_auth_page(browser):
    auth_page.load(browser)

@when('I type in <email> and <password>')
def do_login(browser, email, password):
    auth_page.do_login(browser, email, password)

@then('I should be <result> logged in')
def verify_account_page(browser, result):  
    if result == 'successfully':
        title = auth_page.get_title(browser, auth_page.MY_ACCOUNT_PAGE_TITLE)
        assert title == auth_page.MY_ACCOUNT_PAGE_TITLE
    if result == 'unsuccessfully':
        assert auth_page.is_visible(browser,auth_page.AUTH_ALERT_ERROR)
import random
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from .base_page import BasePage

class AuthPage(BasePage):

    """   LOCATORS   """
    """   For login   """
    CREATE_ACCOUNT_EMAIL_INPUT = (By.ID, "email_create")
    CREATE_ACCOUNT_BUTTON = (By.ID, "SubmitCreate")
    CREATE_ACCOUNT_ERROR = (By.CSS_SELECTOR, "#create_account_error")
    LOGIN_EMAIL_INPUT = (By.ID, "email")
    LOGIN_PASSWORD_INPUT = (By.ID, "passwd")
    LOGIN_BUTTON = (By.ID, "SubmitLogin")
    AUTH_ALERT_ERROR = (By.CSS_SELECTOR, "h1 + .alert-danger")

    """   For account creation   """
    TITLE_MR = (By.CSS_SELECTOR, "#id_gender1")
    TITLE_MRS = (By.CSS_SELECTOR, "#id_gender2")
    FIRST_NAME = (By.CSS_SELECTOR, "#customer_firstname")
    LAST_NAME = (By.CSS_SELECTOR, "#customer_lastname")
    EMAIL = (By.CSS_SELECTOR, "#email")
    PASSWORD = (By.CSS_SELECTOR, "#passwd")
    BIRTH_DATE_DAY = (By.CSS_SELECTOR, "#days")
    BIRTH_DATE_MONTH = (By.CSS_SELECTOR, "#months")
    BIRTH_DATE_YEAR = (By.CSS_SELECTOR, "#years")
    NEWSLETTER_CHECKBOX = (By.CSS_SELECTOR, "[name='newsletter']")
    SPECIAL_OFFERS_CHECKBOX = (By.CSS_SELECTOR, "#optin")
    COMPANY = (By.CSS_SELECTOR, "#company")
    ADDRESS_LINE1 = (By.CSS_SELECTOR, "#address1")
    ADDRESS_LINE2 = (By.CSS_SELECTOR, "#address2")
    CITY = (By.CSS_SELECTOR, "#city")
    STATE = "id_state"
    POSTAL_CODE = (By.CSS_SELECTOR, "#postcode")
    COUNTRY = "id_country"
    ADDITIONAL_INFORMATION = (By.CSS_SELECTOR, "[name='other']")
    HOME_PHONE = (By.CSS_SELECTOR, "#phone")
    MOBILE_PHONE = (By.CSS_SELECTOR, "#phone_mobile")
    ADDRESS_ALIAS = (By.CSS_SELECTOR, "#alias")
    REGISTER_BUTTON = (By.CSS_SELECTOR, "#submitAccount")
    REGISTRATION_ERROR = (By.CSS_SELECTOR, ".alert-danger")

    """   Page titles   """
    AUTH_PAGE_TITLE = "Login - My Store"
    MY_ACCOUNT_PAGE_TITLE = "My account - My Store"

    url = 'http://automationpractice.com/index.php?controller=authentication'

    
    def set_title(self, browser, title):
        if title.upper() == "MR":
            self.do_click(browser, self.TITLE_MR)
        else:
            self.do_click(browser, self.TITLE_MRS)

    def set_first_name(self, browser, name):
        self.do_send_keys(browser, self.FIRST_NAME, name)

    def set_last_name(self, browser, last_name):
        self.do_send_keys(browser, self.LAST_NAME, last_name)

    def set_password(self, browser, password):
        self.do_send_keys(browser, self.PASSWORD, password)
    
    def set_company(self, browser, company):
        self.do_send_keys(browser, self.COMPANY, company)

    def set_address(self, browser, address):
        self.do_send_keys(browser, self.ADDRESS_LINE1, address)

    def set_address_line2(self, browser, address):
        self.do_send_keys(browser, self.ADDRESS_LINE2, address)

    def set_city(self, browser, city):
        self.do_send_keys(browser, self.CITY, city)

    def set_random_state(self, browser):
        element = Select(browser.find_element_by_id(self.STATE))
        element.select_by_index(random.randint(1, 50))

    def set_postal_code(self, browser, postal_code):
        self.do_send_keys(browser, self.POSTAL_CODE, postal_code)

    def set_country(self, browser):
        element = Select(browser.find_element_by_id(self.COUNTRY))
        element.select_by_visible_text("United States")

    def set_additional_information(self, browser, text):
        self.do_send_keys(browser, self.ADDITIONAL_INFORMATION, text)

    def set_home_phone(self, browser, phone):
        self.do_send_keys(browser, self.HOME_PHONE, phone)

    def set_mobile_phone(self, browser, phone):
        self.do_send_keys(browser, self.MOBILE_PHONE, phone)

    def set_address_alias(self, browser, address_alias):
        self.do_send_keys(browser, self.ADDRESS_ALIAS, address_alias)

    def register(self, browser, title, name, last_name, password, address, city, postal_code, phone):
        self.set_title(browser, title)
        self.set_first_name(browser, name)
        self.set_last_name(browser, last_name)
        self.set_password(browser, password)
        self.set_address(browser, address)
        self.set_city(browser, city)
        self.set_random_state(browser)
        self.set_postal_code(browser, postal_code)
        self.set_country(browser)
        self.set_mobile_phone(browser, phone)
        self.do_click(browser, self.REGISTER_BUTTON)


    def do_login(self, browser, email, password):
        self.do_send_keys(browser, self.LOGIN_EMAIL_INPUT, email)
        self.do_send_keys(browser, self.LOGIN_PASSWORD_INPUT, password)
        self.do_click(browser,self.LOGIN_BUTTON)

    def start_creating_account(self, browser, email):
        self.do_send_keys(browser, self.CREATE_ACCOUNT_EMAIL_INPUT, str(random.randint(1, 99999))+email)
        self.do_click(browser,self.CREATE_ACCOUNT_BUTTON)

    

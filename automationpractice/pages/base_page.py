from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class BasePage:

    """   LOCATORS   """
    SIGN_IN = (By.CSS_SELECTOR, ".login")
    SIGN_OUT = (By.CSS_SELECTOR, ".logout")
    PRODUCT_SEARCH_INPUT = (By.CSS_SELECTOR, "#search_query_top")
    PRODUCT_SEARCH_BUTTON = (By.CSS_SELECTOR, "[name='submit_search']")
    SEARCH_WARNING = (By.CSS_SELECTOR, ".alert-warning")


    url = 'http://automationpractice.com/'

    def load(self, browser):
        browser.get(self.url)

    def do_click(self,browser, by_locator):
        WebDriverWait(browser, 10).until(EC.visibility_of_element_located(by_locator)).click()

    def do_send_keys(self, browser, by_locator, text):
        WebDriverWait(browser, 10).until((EC.visibility_of_element_located(by_locator))).send_keys(text)

    def get_title(self, browser, title):
        WebDriverWait(browser, 10).until(EC.title_is(title))
        return browser.title

    def is_visible(self, browser, by_locator):
        element = WebDriverWait(browser, 10).until(EC.visibility_of_element_located(by_locator))
        return bool(element)

    def find_element(self, browser, by_locator):
        element = WebDriverWait(browser, 10).until(EC.visibility_of_element_located(by_locator))
        return element

    def sign_in(self, browser):
        self.do_click(browser, self.SIGN_IN)

    def sign_out(self, browser):
        self.do_click(browser, self.SIGN_OUT)

    def search_product(self, browser, product):
        self.do_send_keys(browser, self.PRODUCT_SEARCH_INPUT, product)
        self.do_click(browser, self.PRODUCT_SEARCH_BUTTON)
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from pages.base_page import BasePage
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class HomePage(BasePage):
    
    """   LOCATORS   """
    PRODUCTS_LIST = ".product_list > li"
    QUICK_VIEW = "a.quick-view"
    QUICK_VIEW_BUTTON = (By.LINK_TEXT, "Quick view")
    LABEL_SUCCESSFULLY_ADDED_PRODUCT = ".layer_cart_product > h2"
    PRODUCT = "div.product-container"
    PRODUCT_NAME = ".product-name"
    PRODUCT_ADD_TO_CART_HOVER = (By.LINK_TEXT, "Add to cart")
    QUICK_VIEW_IFRAME = (By.CLASS_NAME, "fancybox-iframe")
    PRODUCT_VIEW_ADD_TO_CART = (By.CLASS_NAME, "exclusive")
    """   Checkout   """
    PROCEED_TO_CHECKOUT = (By.CSS_SELECTOR, "[title='Proceed to checkout']")
    PROCEED_TO_ADDRESS = (By.XPATH, "//*[@id='center_column']/p[2]/a[1]")
    PROCEED_TO_SHIPPING = (By.XPATH, "//*[@id='center_column']/form/p/button")
    PROCEED_TO_PAYMENT = (By.XPATH, "//*[@id='form']/p/button")
    TERMS_CHECKBOX = (By.ID, "cgv")
    BANK_WIRE_BUTTON = (By.XPATH, "//*[@id='HOOK_PAYMENT']/div[1]/div/p/a")
    CONFIRM_ORDER = (By.XPATH, "//*[@id='cart_navigation']/button")
    ORDER_COMPLETE_INFORMATION = "#center_column > div > p > strong"

    def check_search_results(self, browser, string):
        products = browser.find_elements_by_css_selector(self.PRODUCTS_LIST)

        for product in products:
            if string not in product.find_element_by_css_selector(self.PRODUCT_NAME).text.lower():
                return False
            else:
                continue
        return True

    def hover_on_product(self, browser):
        ActionChains(browser).move_to_element(browser.find_element_by_css_selector(self.PRODUCT)).perform()

    def add_product_to_cart_hover(self, browser):
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable(self.PRODUCT_ADD_TO_CART_HOVER)).click()

    def verify_product_added_successfully(self, browser):
        return len(browser.find_elements_by_css_selector(self.LABEL_SUCCESSFULLY_ADDED_PRODUCT)) > 0

    def open_product_quick_view(self, browser):
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable(self.QUICK_VIEW_BUTTON)).click()

    def add_product_to_cart_quick_view(self, browser):
        WebDriverWait(browser, 10).until(EC.frame_to_be_available_and_switch_to_it(self.QUICK_VIEW_IFRAME))
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable(self.PRODUCT_VIEW_ADD_TO_CART)).click()
        browser.switch_to.default_content()
        
    def proceed_to_checkout(self, browser):
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable(self.PROCEED_TO_CHECKOUT)).click()

    def proceed_to_address(self, browser):
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable(self.PROCEED_TO_ADDRESS)).click()

    def proceed_to_shipping(self, browser):
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable(self.PROCEED_TO_SHIPPING)).click()

    def terms_checkbox_check(self, browser):
        browser.find_element_by_id("cgv").click()

    def proceed_to_payment(self, browser):
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable(self.PROCEED_TO_PAYMENT)).click()

    def choose_bank_wire(self, browser):
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable(self.BANK_WIRE_BUTTON)).click()
    
    def confirm_order(self, browser):
        WebDriverWait(browser, 10).until(EC.element_to_be_clickable(self.CONFIRM_ORDER)).click()

    def verify_order_complete_message(self, browser):
        txt = browser.find_element_by_css_selector(self.ORDER_COMPLETE_INFORMATION).text
        return txt
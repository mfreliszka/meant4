Feature: Login to automationpractice.com

    Scenario Outline: Login
        Given The authentication page is displayed
        When I type in <email> and <password>
        Then I should be <result> logged in

        Examples: using valid data
            |email|password|result|
            |jack_the_ripper@ripper.com|ripper1888|successfully|

        Examples: using invalid email and valid password
            |email|password|result|
            |jack@ripper.com|ripper1888|unsuccessfully|

        Examples: using valid email and invalid password
            |email|password|result|
            |jack_the_ripper@ripper.com|ripper188|unsuccessfully|

        Examples: using valid email and no password
            |email|password|result|
            |jack_the_ripper@ripper.com||unsuccessfully|

        Examples: using no email and valid password
            |email|password|result|
            ||ripper188|unsuccessfully|
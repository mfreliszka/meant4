Feature: Adding product to shopping cart
    
    Scenario: Add product to cart using button on product hover
        Given The home page is displayed
        When I hover on product on page
        And I click Add to cart button
        Then A message Product successfully added should be displayed

    Scenario: Add product to cart using quick view
        Given The home page is displayed
        When I hover on product on page
        And I click Quick view button
        And I click Add to cart button on quick view
        Then A message Product successfully added should be displayed
Feature: Create account on automationpractice.com

    Scenario Outline: Account Creation
        Given The authentication page is displayed
        When I type in <email> address, next I click on Create an account button
        And I set <title>, <name>, <last_name>, <password>, <address>, <city>, <postal_code>, <phone>, next I click Register button
        Then I should <result> register new account

        Examples: using valid data
        |email|title|name|last_name|password|address|city|postal_code|phone|result|
        |jack12345678@theripper.com|MR|Michal|Freliszka|123abc123|Lipowa 99|Warszawa|12312|123123123|successfully|

        Examples: using no name fields
        |email|title|name|last_name|password|address|city|postal_code|phone|result|
        |jack12345678@theripper.com|MRS||Freliszka|123abc123|Lipowa 99|Warszawa|12312|123123123|unsuccessfully|

        Examples: using letters in phone number
        |email|title|name|last_name|password|address|city|postal_code|phone|result|
        |jack12345678@theripper.com|MRS|Michal|Freliszka|123abc123|Lipowa 99|Warszawa|12312|abc123abc|unsuccessfully|

    Scenario: Invalid email account creation
        Given The authentication page is displayed
        When I type in invalid <email> address, next I click on Create an account button
        Then I should see Invalid email error

        Examples: using not all required fields
        |email|
        |valid12345678@|
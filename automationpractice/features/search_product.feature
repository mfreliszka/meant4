Feature: Product searching

    Scenario: Search with empty string
        Given The home page is displayed
        When I click on search button with no string typed
        Then I should see alert window Please enter a search keyword

    Scenario Outline: Search with a string
        Given The home page is displayed
        When I type <string> in search field and click on search button
        Then I should see only products with <string> in title

    Examples: Vertical
        |string|blouse|t-shirt|printed|
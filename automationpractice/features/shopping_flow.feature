Feature: Shopping Flow

    Scenario Outline: Buy product with a logged in user
        Given I am logged in with correct <email> and <password>
        When I go to home page
        And I hover on a product
        And I click on Add to cart button
        And I click Proceed to checkout and go to Cart
        And I click Proceed to checkout and go to Address step
        And I click I click Proceed to checkout and go to Shipping step
        And I click checkbox Terms of service
        And I click Proceed to checkout and go to Payment step
        And I click Pay by bank wire
        And I click confirm my order
        Then A message Your order on My Store is complete should be displayed 

        Examples: using valid data
            |email|password|
            |jack_the_ripper@ripper.com|ripper1888|

    Scenario Outline: Buy product with a not logged in user
        Given The home page is displayed
        When I hover on a product
        And I click on Add to cart button
        And I click Proceed to checkout and go to Cart
        And I click Proceed to checkout and go to Sign in step
        And I login with my <email> and <password> and click on Sign in
        And I click I click Proceed to checkout and go to Shipping step
        And I click checkbox Terms of service
        And I click Proceed to checkout and go to Payment step
        And I click Pay by bank wire
        And I click confirm my order
        Then A message Your order on My Store is complete should be displayed 

        Examples: using valid data
            |email|password|
            |jack_the_ripper@ripper.com|ripper1888|
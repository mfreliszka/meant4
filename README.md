# Python automated tests with pytest and pytest-bdd
This is a project for learning test automation in Python using Selenium, pytest and pytest-bdd. Project consists of two separate modules - automationpractice and restful-booker.

** Table of contents **

[TOC]

## Getting started
These instructions will get you a copy of the project up and running on your local machine.
### Download repository
`git clone https://mfreliszka@bitbucket.org/mfreliszka/meant4.git`
### Automationpractice module:
#### Installing requirements
Use commands below in main repository folder.

`cd automationpractice`

`pip install -r requirements.txt`
#### Running tests
Use command below in automationpractice folder.

`python -m pytest`
### Restful-booker module:
#### Installing requirements
Use commands below in main repository folder.

`cd restful-booker`

`pip install -r requirements.txt`
#### Running tests
Use command below in restful-booker folder.

`python -m pytest`